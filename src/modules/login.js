import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import vuetify from '@/plugins/vuetify';

export default{
	namespaced: true,
	state:{
		login:false,
		datosUsuario:'',
		idcaja:'',
	},

	mutations:{
		LOGEADO(state, value){
			state.login = value
		},

		DATOS_USUARIO(state, datosUsuario){
      state.datosUsuario = datosUsuario
		},

		MIDCAJA(state, idcaja){
      state.idcaja = idcaja
		},

		SALIR(state){
			state.login = false
			state.datosUsuario = ''
		},

		OCULTARTOOLBAR(state, value){
			state.toolbar = value
		}
	},

	actions:{
		// Valida si el usario existe en la BD
		validarUser({commit}, usuario){
			return new Promise((resolve, reject) => {
			 // console.log (usuario)
			  Vue.http.post('sessions', usuario).then(respuesta=>{
			  	return respuesta.json()
			  }).then(respuestaJson=>{
	         // console.log('respuestaJson',respuestaJson)
					if(respuestaJson == null){
						resolve(false) 
					}else{
						resolve(respuestaJson) 
        	}
      	}, error => {
        	reject(error)
      	})
			})
		},

		GetInfoUser({commit, dispatch}, usuario){
			return new Promise((resolve, reject) => {
			  Vue.http.post('sessions', usuario).then(response=>{
	        console.log('response',response.body)
      		commit('DATOS_USUARIO',response.body)
					commit('LOGEADO', true)
					resolve(true)
	    	}, error => {
	      	resolve(false)
	    	})
			})
		},

		getidCaja({commit, dispatch}, idcaja){
			commit('MIDCAJA',idcaja)
		},

		salirLogin({commit}){
			commit('SALIR')
		},

		ocultarToolbar({commit}, value){
			commit('LOGEADO', value)
		}
	},

	getters:{
		getLogeado(state){
		  return state.login
		},

		getdatosUsuario(state){
			return state.datosUsuario
		},

		getIdCaja(state){
			return state.idcaja
		},

		getToolbar(state){
			return state.toolbar
		}

	}
}