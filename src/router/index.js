import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import rutas from '@/router'

import Login from '@/views/login/login.vue'
import Home from '@/views/Home.vue'

// Rutas y Usuarios
import Rutas               from '@/views/rutas_usuarios/Rutas.vue'
import Accesos             from '@/views/rutas_usuarios/Accesos.vue'
import Usuarios            from '@/views/rutas_usuarios/Usuarios.vue'
import RelojChecador       from '@/views/rutas_usuarios/RelojChecador.vue'
import SolicitarVacaciones from '@/views/rutas_usuarios/SolicitarVacaciones.vue'
import VacacionesEmpleados from '@/views/rutas_usuarios/VacacionesEmpleados.vue'
import MiPerfil            from '@/views/rutas_usuarios/MiPerfil.vue'

// Cotizador
import Categorias    from '@/views/cotizador/CatCategoria.vue'
import Unidades      from '@/views/cotizador/Unidades.vue'
import Historial     from '@/views/cotizador/Historial.vue'
import Cotizador     from '@/views/cotizador/Cotizador.vue'
import Articulos     from '@/views/cotizador/CatProductos.vue'


// CORTE CAJA
import Corte         from '@/views/corte/Home.vue'
import Dia           from '@/views/corte/reportes/dia.vue'
import Mes           from '@/views/corte/reportes/mes.vue'
import Estatuscaja   from '@/views/corte/estatuscaja.vue'

// FORMATOS
import Formatos      from '@/views/formatos/Formatos.vue'

// ADMINISTRACIÓN DE ARCHIVOS
import AdministrarAccesos   from '@/views/archivos/AdministrarAccesos.vue'
import Carpetas             from '@/views/archivos/Carpetas.vue'

// REQUISIONES
import Departamentos               from '@/views/requisiciones/Departamentos.vue'
import Puestos                     from '@/views/requisiciones/Puestos.vue'
import Requisiciones               from '@/views/requisiciones/Requisiciones.vue'
import MisRequisiciones            from '@/views/requisiciones/MisRequisiciones.vue'
import RequisicionesDepartamento   from '@/views/requisiciones/RequisicionesDepartamento.vue'
import UsuariosRequi               from '@/views/requisiciones/Usuarios.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    //MODULO DE LOGIN
    { path: '/', name: 'login' , component: Login, 
      meta: { libre: true, ADMIN: true, USUARIO: true, SUPERVISOR: true} },
    { path: '/home', name: 'home' , component: Home, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },


    { path: '/rutas', name: 'Rutas' , component: Rutas, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },
    
    { path: '/accesos', name: 'Accesos' , component: Accesos, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },
    
    { path: '/usuarios', name: 'Usuarios' , component: Usuarios, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },

    { path: '/MiPerfil', name: 'MiPerfil' , component: MiPerfil, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },

    { path: '/solicitarvacaciones', name: 'SolicitarVacaciones' , component: SolicitarVacaciones, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },

    { path: '/vacacionesempleados', name: 'VacacionesEmpleados' , component: VacacionesEmpleados, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },

    // COTIZADOR

    { path: '/categorias', name: 'Categorias' , component: Categorias, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },
    
    { path: '/unidades'  , name: 'Unidades'   , component: Unidades, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },
    
    { path: '/historial', name: 'Historial'   , component: Historial, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },

    { path: '/cotizador', name: 'Cotizador'   , component: Cotizador, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },
    
    { path: '/articulos', name: 'Articulos'   , component: Articulos, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true} },

    // CORTEE
    { path: '/corte', name: 'corte' , component: Corte, 
      meta: { ADMIN: true, USUARIO: true} },

    { path: '/dia'  , name: 'dia' , component: Dia, 
      meta: { ADMIN: true, USUARIO: true} },

    { path: '/mes'  , name: 'mes' , component: Mes, 
      meta: { ADMIN: true, USUARIO: true} },

    { path: '/estatuscaja'  , name: 'Estatuscaja' , component: Estatuscaja, 
      meta: { ADMIN: true, USUARIO: true} },
    
    // FORMATOS
    { path: '/formatos'  , name: 'Formatos' , component: Formatos, 
      meta: { ADMIN: true, USUARIO: true} },


    // ADMINSITRACION DE ARCHIVOS
    { path: '/administraraccesos' , name: 'AdministrarAccesos' , component: AdministrarAccesos, 
      meta: { ADMIN: true, SUPERVISOR: true }},
    
    { path: '/carpetas'            , name: 'Carpetas'            , component: Carpetas, 
      meta: { ADMIN: true, USUARIO: true, SUPERVISOR: true }},


    // REQUISICIONES

    { path: '/departamentos'                  , name: 'Departamentos'              , component: Departamentos, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/puestos'                        , name: 'Puestos'                    , component: Puestos, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/requisiciones'                  , name: 'Requisiciones'              , component: Requisiciones, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/misrequisiciones'               , name: 'MisRequisiciones'           , component: MisRequisiciones, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/usuariosrequi'                  , name: 'UsuariosRequi'              , component: UsuariosRequi, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/requisicionesdepartamento'      , name: 'RequisicionesDepartamento'  , component: RequisicionesDepartamento, 
      meta: { ADMIN: true, USUARIO: true }},

    { path: '/relojchecador'                  , name: 'RelojChecador'              , component: RelojChecador, 
      meta: { ADMIN: true, USUARIO: true }},
    
  ]
})

router.beforeEach( (to, from, next) => {
  if(to.matched.some(record => record.meta.libre)){
    next()
  }else if(store.state.login.datosUsuario.admin === 'ADMIN'){
    if(to.matched.some(record => record.meta.ADMIN)){
      next()
    }
  }else if(store.state.login.datosUsuario.admin === 'USUARIO'){
    if(to.matched.some(record => record.meta.USUARIO)){
      next()
    }
  }else if(store.state.login.datosUsuario.admin === 'SUPERVISOR'){
    if(to.matched.some(record => record.meta.SUPERVISOR)){
      next()
    }
  }else{
    next({
      name: 'login'
    })
  }
})

export default router

